package com.example.mobappfinal

data class UserInfo(
    val email: String = "",
    val name: String = "",
    val surname: String = "",
    val phone: String = "",
    val uid: String = "",
    val url: String = "",
)