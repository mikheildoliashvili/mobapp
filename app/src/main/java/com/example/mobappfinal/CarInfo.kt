package com.example.mobappfinal

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class CarInfo(
    val photourl: String = "",
    val fullname: String = "",
    val uid: String = "",
    val carname: String = "",
    val cardetails: String = "",
    val phonenumber: String = "",
    val carprice: String = "",
    val keyid: String ="",
)
