package com.example.mobappfinal.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.mobappfinal.R
import com.google.firebase.auth.FirebaseAuth

class AuthorisationFragment : Fragment(R.layout.authorisation_fragment) {
    private lateinit var email: EditText
    private lateinit var password: EditText
    private lateinit var loginBtn: Button
    private lateinit var register: Button
    private lateinit var reset: TextView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.authorisation_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        init()
        registerListeners()
        listener()
    }

    private fun init() {
        email = view?.findViewById(R.id.email)!!
        password = view?.findViewById(R.id.password)!!
        loginBtn = view?.findViewById(R.id.login_button)!!
        register = view?.findViewById(R.id.newAcc)!!
        reset = view?.findViewById(R.id.passReset)!!
    }

    private fun registerListeners() {

        register.setOnClickListener {
            Navigation.findNavController(requireView())
                .navigate(AuthorisationFragmentDirections.actionAuthorisationFragmentToRegistrationFragment())
        }
        reset.setOnClickListener {
            Navigation.findNavController(requireView())
                .navigate(AuthorisationFragmentDirections.actionAuthorisationFragmentToResetPasswordFragment())
        }
    }

    private fun listener() {
        loginBtn.setOnClickListener {

            val email = email.text.toString().lowercase()
            val password = password.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(activity, "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                            findNavController().navigate(AuthorisationFragmentDirections.actionAuthorisationFragmentToHomeFragment())
                    } else {
                        Toast.makeText(activity, "Halo", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }



}