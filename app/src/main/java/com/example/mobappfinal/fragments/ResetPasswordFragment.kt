package com.example.mobappfinal.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.mobappfinal.R
import com.google.firebase.auth.FirebaseAuth

class ResetPasswordFragment: Fragment(R.layout.reset_password_fragment) {
    private lateinit var email: EditText
    private lateinit var sendBtn: Button
    private lateinit var exitBtn: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        email = view.findViewById(R.id.email)
        sendBtn = view.findViewById(R.id.sendBtn)
        exitBtn = view.findViewById(R.id.exit)

        exitBtn.setOnClickListener {
            Navigation.findNavController(requireView()).navigate(ResetPasswordFragmentDirections.actionResetPasswordFragmentToAuthorisationFragment())
        }

        registerListeners()
    }

    private fun registerListeners() {
        sendBtn.setOnClickListener {
            val email = email.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(activity, "Empty mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(activity, "Check Email!", Toast.LENGTH_SHORT).show()
                        Navigation.findNavController(requireView()).navigate(ResetPasswordFragmentDirections.actionResetPasswordFragmentToAuthorisationFragment())
                    } else {
                        Toast.makeText(activity, "Error!", Toast.LENGTH_SHORT).show()
                    }
                }

        }

    }

}