package com.example.mobappfinal.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.mobappfinal.CarInfo
import com.example.mobappfinal.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class HomeFragment : Fragment(R.layout.home_fragment) {
    private lateinit var recyclerview: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        getPosts()
        if (FirebaseAuth.getInstance().currentUser == null) {
            Navigation.findNavController(requireView())
                .navigate(HomeFragmentDirections.actionHomeFragmentToAuthorisationFragment())
        }
    }

    private fun init() {
        recyclerview = view?.findViewById(R.id.recyclerView)!!
    }

    private fun getPosts() {
        val ref = FirebaseDatabase.getInstance().getReference("carPosts")
        ref.addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val adapter = GroupieAdapter()
                snapshot.children.forEach {
                    val carposts = it.getValue(CarInfo::class.java)
                    adapter.add(CarPosts(carposts!!))
                    adapter.setOnItemClickListener { item, view ->

                        val argument =
                            view.findViewById<TextView>(R.id.textViewHidden).text.toString()
                        val action =
                            HomeFragmentDirections.actionHomeFragmentToCarDetailedFragment(
                                argument
                            )
                        findNavController().navigate(action)
                    }
                    recyclerview.adapter = adapter
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })
    }


    class CarPosts(
        val carPost: CarInfo
    ) : Item<GroupieViewHolder>() {

        override fun getLayout() = R.layout.car_post_design

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.findViewById<TextView>(R.id.textViewCarTitle).text =
                carPost.carname
            viewHolder.itemView.findViewById<TextView>(R.id.textViewHidden).text =
                carPost.keyid
            viewHolder.itemView.findViewById<TextView>(R.id.textViewCarDetail).text =
                "$ " + carPost.carprice
            val image = viewHolder.itemView.findViewById<ImageView>(R.id.imageViewRame)
            if (viewHolder.itemView.context != null) {
                Glide.with(viewHolder.itemView.context).load(carPost.photourl)
                    .transform(RoundedCorners(32))
                    .into(image)
            } else{
                return
            }
        }
    }
}