package com.example.mobappfinal.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.mobappfinal.CarInfo
import com.example.mobappfinal.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class NewCarFragment : Fragment(R.layout.fragment_new_car) {
    private lateinit var photoUrl: EditText
    private lateinit var fullName: EditText
    private lateinit var carName: EditText
    private lateinit var carDetails: EditText
    private lateinit var phoneNumber: EditText
    private lateinit var addPhoto: Button
    private lateinit var addCar: Button
    private lateinit var imageView: ImageView
    private lateinit var carPrice: EditText
    private val auth = FirebaseAuth.getInstance().uid.toString()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_new_car, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        registerListeners()
    }

    private fun init() {
        photoUrl = view?.findViewById(R.id.editTextUrl)!!
        fullName = view?.findViewById(R.id.editTextFullName)!!
        carName = view?.findViewById(R.id.editTextCarInfo)!!
        carDetails = view?.findViewById(R.id.editTextCarDetails)!!
        phoneNumber = view?.findViewById(R.id.editTextPhone)!!
        addPhoto = view?.findViewById(R.id.AddPhoto)!!
        addCar = view?.findViewById(R.id.AddCar)!!
        imageView = view?.findViewById(R.id.imageView2)!!
        carPrice = view?.findViewById(R.id.editTextCarPrice)!!

    }

    private fun registerListeners() {
        addPhoto.setOnClickListener {
            val photourl = photoUrl.text.toString()
            if (photourl.isNotEmpty()) {
                Glide.with(this@NewCarFragment).load(photourl).transform(RoundedCorners(32))
                    .into(imageView)
            } else {
                Toast.makeText(activity, "photo field is empty", Toast.LENGTH_SHORT).show()
            }

        }
        addCar.setOnClickListener {
            val photourl = photoUrl.text.toString()
            val fullname = fullName.text.toString()
            val carname = carName.text.toString()
            val cardetails = carDetails.text.toString()
            val phonenumber = phoneNumber.text.toString()
            val carprice = carPrice.text.toString()
            if (fullname.isNotEmpty() && carname.isNotEmpty() && cardetails.isNotEmpty() && phonenumber.isNotEmpty() && phonenumber.startsWith(
                    "5"
                )

            ) {
                val ref = FirebaseDatabase.getInstance().getReference("carPosts").push()

                val carInfo = CarInfo(
                    photourl,
                    fullname,
                    auth,
                    carname,
                    cardetails,
                    phonenumber,
                    carprice,
                    ref.key!!
                )
                ref.setValue(carInfo).addOnSuccessListener {
                    val action = NewCarFragmentDirections.actionNewCarFragmentToHomeFragment()
                    findNavController().navigate(action)
                    Toast.makeText(
                        activity,
                        "Your car post has been successfully added",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    activity,
                    "You must fill out all of the text fields!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}