package com.example.mobappfinal.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.mobappfinal.R
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeFragment : Fragment(R.layout.password_change_fragment) {
    private lateinit var oldPassword: EditText
    private lateinit var newPassword: EditText
    private lateinit var repeatPassword: EditText
    private lateinit var saveBtn: Button
    private lateinit var exitBtn: Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        oldPassword = view.findViewById(R.id.old_password)
        newPassword = view.findViewById(R.id.new_password)
        repeatPassword = view.findViewById(R.id.repeat_password)
        saveBtn = view.findViewById(R.id.save_button)
        exitBtn = view.findViewById(R.id.exit_button)

        registerButtons()
        registerListeners()
    }

    private fun registerListeners() {
        saveBtn.setOnClickListener {
            val newPassword = newPassword.text.toString()
            val oldPassword = oldPassword.text.toString()
            val repeatPassword = repeatPassword.text.toString()

            if (newPassword.isEmpty() || newPassword.length < 7) {
                Toast.makeText(activity, "Incorrect password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (oldPassword.isEmpty() || oldPassword.length < 7) {
                Toast.makeText(activity, "Incorrect password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (repeatPassword.isEmpty() || repeatPassword.length < 7) {
                Toast.makeText(activity, "Incorrect password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (newPassword != repeatPassword) {
                Toast.makeText(activity, "password aren't equal", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (newPassword == oldPassword) {
                Toast.makeText(
                    activity,
                    "new password must be different from old",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show()
                        Navigation.findNavController(requireView())
                            .navigate(PasswordChangeFragmentDirections.actionPasswordChangeFragmentToHomeFragment())
                    } else {
                        Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

    private fun registerButtons() {
        exitBtn.setOnClickListener() {
            Navigation.findNavController(requireView())
                .navigate(PasswordChangeFragmentDirections.actionPasswordChangeFragmentToProfileFragment())
        }
    }

}