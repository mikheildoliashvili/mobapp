package com.example.mobappfinal.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.mobappfinal.CarInfo
import com.example.mobappfinal.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class CarDetailedFragment : Fragment(R.layout.fragment_car_detailed) {
    private lateinit var imageView: ImageView
    private lateinit var price: TextView
    private lateinit var fullName: TextView
    private lateinit var manufacturer: TextView
    private lateinit var details: TextView
    private lateinit var phone: TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_car_detailed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        getDetailedPost()
    }

    private fun init() {
        imageView = view?.findViewById(R.id.imageView5)!!
        details = view?.findViewById(R.id.textView3)!!
        manufacturer = view?.findViewById(R.id.textViewCarManu)!!
        price = view?.findViewById(R.id.textViewPrice)!!
        fullName = view?.findViewById(R.id.textViewName)!!
        phone = view?.findViewById(R.id.textViewNumber)!!
    }

    private fun getDetailedPost() {
        val hidden =
            CarDetailedFragmentArgs.fromBundle(requireArguments()).argument
        FirebaseDatabase.getInstance().getReference("carPosts").child(hidden)
            .addListenerForSingleValueEvent(object : ValueEventListener {

                override fun onDataChange(snapshot: DataSnapshot) {
                    val getInfo =
                        snapshot.getValue(CarInfo::class.java)
                    details.setText(getInfo?.cardetails.toString())
                    manufacturer.setText(getInfo?.carname.toString())
                    price.setText("Car Price is : $ " + getInfo?.carprice.toString())
                    fullName.setText("Name : " + getInfo?.fullname.toString())
                    phone.setText("Phone Number : " + getInfo?.phonenumber.toString())
                    Glide.with(this@CarDetailedFragment).load(getInfo?.photourl.toString())
                        .transform(RoundedCorners(32)).into(imageView)
                }

                override fun onCancelled(error: DatabaseError) {
                }

            })
    }
}