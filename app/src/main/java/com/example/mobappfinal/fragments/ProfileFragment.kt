package com.example.mobappfinal.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.mobappfinal.R
import com.example.mobappfinal.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.w3c.dom.Text

class ProfileFragment : Fragment(R.layout.profile_fragment) {
    private lateinit var saveBtn: Button
    private lateinit var photo: ImageView
    private lateinit var url: EditText
    private lateinit var mailInfo: TextView
    private lateinit var phone: TextView
    private lateinit var fullName: TextView
    private lateinit var logout: TextView
    private lateinit var addPostButton: Button
    private lateinit var passChng: Button
    private val auth = FirebaseAuth.getInstance()
    private val dataBase = FirebaseDatabase.getInstance().getReference("userInfo")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        listener()
        dataBase.child(auth.currentUser?.uid!!)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val userInfo: UserInfo = snapshot.getValue(UserInfo::class.java) ?: return
                    fullName.text = userInfo.name + " " + userInfo.surname
                    mailInfo.text = userInfo.email
                    phone.text = userInfo.phone
                    if (this@ProfileFragment != null) {
                        Glide.with(this@ProfileFragment).load(userInfo.url).transform(CircleCrop())
                            .into(photo)
                    } else {
                        return
                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })

    }

    private fun init() {
        photo = view?.findViewById(R.id.photo)!!
        mailInfo = view?.findViewById(R.id.mailInfo)!!
        phone = view?.findViewById(R.id.phone)!!
        fullName = view?.findViewById(R.id.fullName)!!
        logout = view?.findViewById(R.id.logout)!!
        addPostButton = view?.findViewById(R.id.addPost)!!
    }

    private fun listener() {
        logout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val action = ProfileFragmentDirections.actionProfileFragmentToAuthorisationFragment()
            findNavController().navigate(action)
        }
        addPostButton.setOnClickListener {
            val action = ProfileFragmentDirections.actionProfileFragmentToNewCarFragment()
            findNavController().navigate(action)

        }

    }


}