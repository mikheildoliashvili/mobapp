package com.example.mobappfinal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val bottomNavigationMenu = findViewById<BottomNavigationView>(R.id.bottomNavMenu)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val controller = navHostFragment.navController
        bottomNavigationMenu.setupWithNavController(navController = controller)


        controller.addOnDestinationChangedListener{ navController: NavController, navDestination: NavDestination, bundle: Bundle? ->

            when (navDestination.id) {
                R.id.authorisationFragment , R.id.registrationFragment,R.id.resetPasswordFragment, R.id.passwordChangeFragment-> bottomNavigationMenu.visibility =
                    View.GONE

                else -> bottomNavigationMenu.visibility = View.VISIBLE

            }

        }
    }
}