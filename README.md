### Name ###

    Car Shop

### Description ###

    This app is for buying and selling cars.
        1) you can post your own cars for selling and also buy them from this app.
        2) to access this app portal you have simply to register, and manage your own account.

### Instalation ###

    To get started you have to simply clone this repository to android studio.

### Usage ###

	Application is based on 2 primary parts, first part is login, registration and password reset page
	1) Registation 
	 		If clients dont have acccount they need to register
			Requirements:
				1) Phone Number (Must contain only digits)
				2) Name, Last Name 
				3) Mail (It must be valid)
				4) Password, Password Confirm (Password must contain at least 9 symbols, You have to use digits and letters at the same time while choosing password)
				5) Photo URL (Copy image address)
				
	2) Log In
	 		To start using app clients have to login. 
			You need to use your email and password to register and then to log into your account.
	3) Reset Password
			If clients forgot password they can reset it using their emails.
			If you forgot your password you must enter email into reset password page.
			
	After this starts the second part of application (client service)
	
### App Analyze ###

	1) Firebase Authentification(Log IN, Registration, Password Reset)
	
	2) Firebase Realtime Database
	
	3) Groupie Addapter
	
	4) Glide Library

### Support ###


	1) If you have any kind of sort problem about our app contact us: 599-759-924(bugfix).

 	2) for any question that will improve our service for you and our dear customers contact us : 551-58-01-82 (DevService).
	
### Authors ###

    Giorgi Kapanadze & Mikheil Doliashvili
	
### Project Status ###

	BetaVersion 🙂